# testcase-fjrodriguez

## Ramas actuales de la solución
```
-Main
-qa
-dev
```

## Levantar proyecto 
```
#backend 
cd backend
npm install 
npm start
npm test #para pruebas

#frontend
cd frontend
npm install
npm start #frontend tambien levanta en puerto 3000, typear Y en consola para que levante en 3001
```

## Implementación API 


Libreria winston - registro de logs: se implementó unicamente en las llamadas hacia el API externa por tiempo y simplicidad del ejercicio, pero yo registraria un incoming request detectando IP, usuario, requestbody, VM/Node de ejecución etc...

Libreria axios - llamadas http: libreria para poder llamar a servicios externos bajo protocolo http o https

Libreria papaparse - parseo de cadenas strings: libreria para poder hacer mas ágil el parseo de documentos 

Libreria swagger - documentación: genera un resumen de los endpoints que identifica dentro del archivo routes para facilitar la construccion del documento swagger.

Estructura de carpetas

```
- constants: definicion de archivos de constantes para no dejar harcodeado datos como strings o números o identificadores
- log: carpeta para almacenamiento de logs, no se incluyo en el .gitignore para dejar registro de logs al momento de la ejecucion del ejercicio, pero si se recomienda incluir en el archivo.
- routes: carpeta para separar routes, en donde se colocan solamente la definicion de las rutas y el llamado hacia los controladores (o servicios)
- services: carpeta para separar archivos de logica de servicio, recomendable tener un archivo por entidad o caso de uso, mismo para archivo de routes
- utils: carpeta para almacenar funciones generales o que puedan ser reutilizadas en otras funciones de la capa de servicio, acá se podria separa incluso por casos de uso o por metodos que centralicen conexiones a un ORM/DB o funcionalidades especificas que deban de ser generales.
- config.json: archivo para colocar definiciones basicas de configuracion, basado en el ambiente se pudiesen tomar configuraciones para cada rama: dev, qa, main
- index.js: definicion basica de un API en express, acá se pudiese implementar una llamada a un middleware que valide y autorice basado en un metodo de autenticacion la request entrante. tambien es aconsejable restringir los CORS para permitir llamadas unicamente del frontend autorizado en ambientes como qa, main 
- package.json: definicion de las dependencias
- swagger.js y swaggerDefinition.js:  implementación de documentación del API creada, para poder tener bajo la direcion http://localhost:3000/docs el detalle de los servicios
```

## Implementación Frontend

Libreria bootstrap - base html: libreria para construcción de componentes básicos del html

Libreria axios - llamadas http: libreria para poder llamar a servicios externos bajo protocolo http o https

Estructura de carpetas

```
- public: archivos assets generales para el sitio
- src/components: componentes para la construcción del sitio. Cada componente contiene su .css y su .js 
- src: contiene archivos base del proyecto como App e Index con la implementación de los componentes, tambien contiene logos y otros archivos de pruebas que ya no me dio tiempo de implementar. 
```