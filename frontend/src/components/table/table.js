import 'bootstrap/dist/css/bootstrap.css';

import React from 'react';
import { useState, useEffect } from 'react';
import axios  from 'axios';

function Table() {

  const [data, setData] = useState([]);

  const callService = () => {
    axios.get('http://localhost:3000/files/data').then((response) => {
      const arrFiles = response.data;
      const lstData = [];
      for(const file of arrFiles)
      {
        for(const lines of file.lines)
        {
          const objFile = {
            fileName: file.file,
            text: lines.text,
            number: lines.number,
            hex: lines.hex
          };
          lstData.push(objFile);
        }
      }
      setData(lstData);
    });
  }

  useEffect(() => {
    callService();
  }, []);

  return (
    <table className="table">
      <thead>
        <tr>
          <th>File Name</th>
          <th>Text</th>
          <th>Number</th>
          <th>Hex</th>
        </tr>
      </thead>
      <tbody>
      { 
        data && data.map(item => (
            <tr>
                <td>{item.fileName}</td>
                <td>{item.text}</td>
                <td>{item.number}</td>
                <td>{item.hex}</td>
            </tr>
        ))
      }
      </tbody>
    </table>
  )
}

export default Table;