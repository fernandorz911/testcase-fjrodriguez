import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

import Navbar from './components/navbar/navbar';
import Table from './components/table/table';

function App() {
  return (
    <div className="App">
      <Navbar />
      <br />
      <br />
      <Table />
    </div>
  );
}

export default App;
