const Papa = require('papaparse')
var fGen = require("../utils/fGen");
const { response } = require('express');

/* FUNCION PRINCIPAL DE GETFILES PARA LLAMAR DENTRO DEL ROUTES 
recibe opcional un parametro 'file' el que utiliza para buscar especificamente el contenido de un archivo
si no recibe el parametro, buscará y procesará todos los archivos bajo las reglas especificadas.
*/
async function getFiles(req, res) {
    try {
    let fileParam = req.query.file;
    // console.log(fileParam)
    let responseFiles = await fGen.callService("getFiles", null)
    let responseData = [];
    
    for(const fileName of responseFiles.files){
        if(fileParam && fileParam.includes(fileName)){
            const objFile = {};
            let responseProcessFile = await processFile(fileName);
            objFile.file = fileName;
            objFile.lines = responseProcessFile;
            responseData.push(objFile)
        }else if (!fileParam){
            const objFile = {};
            let responseProcessFile = await processFile(fileName);
            objFile.file = fileName;
            objFile.lines = responseProcessFile;
            responseData.push(objFile)
        }
    }


    return fGen.getJsonOK(res, responseData);
    } catch (error) {
        console.log(error)
    }
}

//FUNCION PARA EL PROCESADO DEL ARCHIVO, EN DONDE SE PROCESA UN ARCHIVO INDIVIDUAL 
//Y SE PARSEA SU CONTENIDO, CONSTRUYENDO EL OBJETO SOLICITADO
async function processFile(fileName){
    try {
        let responseFile = await fGen.callService("getFileByName", fileName);
        
        let parsedFile = parseFile(responseFile);
        
        return parsedFile;
    } catch (error) {
        return [];
    }
}

/*FUNCION PARA EL PARSEO DE ARCHIVOS, EN DONDE SE IDENTIFICAN LAS SIGUIENTES REGLAS
si el string trae errores, se devuelve un array vacio para el file
si el string no trae datos, se devuelve un array vacio para el file
si el string no trae los headers, se devuelve un array vacio para el file

se ignoran las filas que no cumplen con la cantidad de columnas tex, number, hex
se ignoran las filas en donde la columna hex, no trae 32 caracteres

se construye el objeto json, obviando el atributo/key file

se retorna el objeto json con el formato solicitado
*/
function parseFile(csvString) {
    const { data, errors } = Papa.parse(csvString);

    if (errors.length > 0) {
        return [];
    }

    if (data.length === 0) {
        return [];
    }

    //Validar si no trae los headers
    if (!(data[0][0] === 'file' && data[0][1] === 'text' && data[0][2] === 'number' && data[0][3] === 'hex')) { 
        return [];
    }

    const headers = data.shift();

    const validRows = data.filter((row) => {
        //ignorar filas que no cumplen con la cantidad de columnas
        if (row.length !== headers.length) { 
            return false;
        }
        //si no tiene 32 digitos el hex
        if (row[headers.indexOf('hex')].length !== 32) { 
            return false;
        }
        return true;
    });

    // genero el objeto
    const result = validRows.map((row) => {
        const obj = {};
        for (let i = 0; i < headers.length; i++) {
            if(headers[i] == "file")
                continue;
            obj[headers[i]] = row[i];
        }
        return obj;
    });

    return result;
};

//FUNCION UTILIZADA PARA LLAMAR AL SERVICIO GET FILES Y DEVOLVER UNICAMENTE EL LISTADO DE ARCHIVOS (PUNTO OPCIONAL DE API)
async function getFileList(req, res) {
    try {
    
    let responseFiles = await fGen.callService("getFiles", null)
    
    return fGen.getJsonOK(res, responseFiles);
    } catch (error) {
        console.log(error)
    }
}
  
  module.exports = {
    getFiles, getFileList, parseFile
  };