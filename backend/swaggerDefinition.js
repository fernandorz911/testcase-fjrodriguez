const swaggerDefinition = {
    openapi: "3.0.0",
    info: {
      title: "API Test Files - Fjrodriguez",
      version: "1.0.0",
      description: "Documentación de la API",
    },
    servers: [
      {
        url: "http://localhost:3000",
      },
    ],
  };
  
  module.exports = swaggerDefinition;