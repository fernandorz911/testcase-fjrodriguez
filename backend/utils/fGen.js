const fs = require('fs');
const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
const axios = require('axios');
const winston = require('winston');
const servicesConfig = require('../config.json').services;
var cResponses = require("../constants/cResponses");

/*Implementacion de funciones generales para reutilizar en otros servicios.
*/
const logger = winston.createLogger({
    transports: [
      new winston.transports.File({
        filename: 'log/errors.log'
      })
    ]
  });

/*SE AGRUPAN TODAS LAS FUNCIONES PARA PODER SER REUTILIZADAS BAJO UNA VARIABLE A EXPORTAR LLAMADAS FGEN
acá se implementa el logger para dejar registro de las solicitudes fallidas que surgan dentro de las ejecuciones
*/
var fGen = {
    callService: function callService(route, params) {
        const headers = {
          Authorization: `Bearer ${servicesConfig.bearer}`
        };
        let url = `${servicesConfig.host}${servicesConfig.urls[route]}`;

        if (params) {
          url = url.replace('{file}', params);
        }
      
        return axios.get(url, { headers })
          .then(response => response.data)
          .catch(error => {
            logger.error(`Error al llamar al servicio '${route}': ${error}`);
            throw error;
          });
    },
    getJsonError: function (res) {
        return res.json({
            estado: cResponses.errorConexionServidor.code,
            mensaje: cResponses.errorConexionServidor.message
        })
    },
    getJsonOK: function (res, data) {
        return res.status(200).json(
            data
        );
    }
}



module.exports = fGen;