'use strict';

//CONSTANTES PARA PREVENIR EL USO DE PALABRAS O IDENTIFICADORES QUE PUEDAN SER REUTILIZADOS.
let cRespuestaEstado = {
    ok: { code: 0, message: 'Ok' },
    errorConexionServidor: { code: 1, message: 'Error del lado del servidor por una excepción.' },
};

module.exports = Object.freeze(cRespuestaEstado); // freeze prevents changes by users