const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const axios = require('axios');
const fGen = require('../utils/fGen');


describe('fGen.callService()', () => {

    //prueba 1: sobre servicio basico 
    it('RETORNA EL LISTADO DE ARCHIVOS CORRECTAMENTE', async () => {
      const route = 'getFiles';
      const expectedData = {
        "files": [
            "test1.csv",
            "test2.csv",
            "test3.csv",
            "test18.csv",
            "test4.csv",
            "test5.csv",
            "test6.csv",
            "test9.csv",
            "test15.csv"
        ]
    };
      let responseFiles = await fGen.callService("getFiles", null)
      expect(responseFiles).to.deep.equal(expectedData);
    });
  
    //Prueba 2: sobre servicio que devuelve datos de un archivo
    it('RETORNA UNA ESTRUCTURA BASICA DEFINIDA CORRECTA', async () => {
      const route = 'getFileByName';
      const fileName = 'test3.csv'

      const expectedData = `file,text,number,hex
test3.csv,vPQlo
test3.csv,jjhvRShpm,713223831,b0f0bd26468692a42b597d9804e99c24
test3.csv,xRhGTqIme,050,f9961506d735c51ca1c5e99884faba6e
test3.csv,thuAOh,5000,7f6cb5b815d98ce3642172d1c17e4930`;
      
      const actualData = await fGen.callService(route, fileName);
      
      expect(actualData).to.deep.equal(expectedData);
    });
  
    it('VALIDA ERROR AL LLAMAR A UNA RUTA NO VALIDA', async () => {
      const route = 'notValid';
    
      let res = {}
    
      try {
        res = await fGen.callService(route, null);
      } catch (error) {
        res = error
        // console.log(res)
      }
    
      expect(res).to.have.property('response');
      expect(res.response.status).to.equal(404);
    });
  });
  