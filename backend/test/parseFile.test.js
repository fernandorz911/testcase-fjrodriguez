const { expect } = require('chai');
const parseFile = require('../services/getFiles');

describe('parseFile', () => {
  it('RETORNAR ARRAY VACIO DADO QUE ES UNA CADENA NO VALIDA', () => {
    const csvString = 'este string no es valido';
    const result = parseFile.parseFile(csvString);
    expect(result).to.be.an('array').that.is.empty;
  });

//   it('RETORNAR ARRAY VACIO DADO QUE LA CADENA NO CONTIENE CARACTERES', () => {
//     const csvString = '';
//     const result = parseFile.parseFile(csvString);
//     expect(result).to.be.an('array').that.is.empty;
//   });

  it('RETORNAR ARRAY VACIO DADO QUE LA CADENA NO CONTIENE DATOS VALIDOS EN LA 1RA FILA', () => {
    const csvString = 'file,text,number,invalid\nvalue,value,value,value';
    const result = parseFile.parseFile(csvString);
    expect(result).to.be.an('array').that.is.empty;
  });

  it('should return valid data from csvString', () => {
    const csvString = 'file,text,number,hex\ntest2.csv,jIJhz,0x1234abcd,\ntest2.csv,BEqNpKGFrBGZPxseaspqTzwC,5543831624,900301a0af61bfd2aec30e783654c545';
    const result = parseFile.parseFile(csvString);
    // console.log(result)
    const expectedResult = [
      { 'text': 'BEqNpKGFrBGZPxseaspqTzwC', 'number': '5543831624', 'hex': '900301a0af61bfd2aec30e783654c545' },
    ];
    expect(result).to.deep.equal(expectedResult);
  });

});
