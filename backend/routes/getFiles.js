const express = require('express');
const router = express.Router();
const fs = require('fs');
const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
const { getFiles, getFileList } = require('../services/getFiles');
const swaggerUi = require('swagger-ui-express');
const swaggerSpec = require('../swagger');

//Definicion de cada endpoint
//Basado en urls del archivo config.server
/**
 * @swagger
 * /getFileList:
 *   get:
 *     summary: Obtiene una lista de archivos disponibles
 *     description: Retorna una lista de archivos disponibles en el servidor.
 *     parameters:
 *       - in: query
 *         name: file
 *         description: Nombre del archivo a consultar
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Lista de archivos disponibles
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   file:
 *                     type: string
 *                     description: Nombre del archivo
 *                   lines:
 *                     type: array
 *                     items:
 *                       type: object
 *                       properties:
 *                         text:
 *                           type: string
 *                           description: Texto de la línea
 *                         number:
 *                           type: string
 *                           description: Número de la línea
 *                         hex:
 *                           type: string
 *                           description: Valor hexadecimal de la línea
 *       400:
 *         description: Error
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Error'
 * components:
 *   schemas:
 *     Error:
 *       required:
 *         - code
 *       properties:
 *         code:
 *           type: string
 *           description: Código del error segun el archivo constants
 *         message:
 *           type: string
 *           description: Mensaje del error según el archivo constants         
 */
router.get(config.server.getFileUrl, async (req, res) => {
  const response = await getFiles(req, res);
  return response;
});

/**
 * @swagger
 * /files/list:
 *   get:
 *     summary: Obtiene una lista de archivos disponibles
 *     description: Retorna una lista de archivos disponibles en el servidor. Se puede filtrar por nombre de archivo mediante el parámetro opcional `file`.
 *     parameters:
 *       - in: query
 *         name: file
 *         schema:
 *           type: string
 *         description: Nombre del archivo a filtrar.
 *     responses:
 *       200:
 *         description: Lista de archivos disponibles
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 files:
 *                   type: array
 *                   items:
 *                     type: string
 *       400:
 *         description: Error
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Error'
 */
router.get(config.server.getFileList, async (req, res) => {
  const response = await getFileList(req, res);
  return response;
});

router.use("/docs", swaggerUi.serve);
router.get("/docs", swaggerUi.setup(swaggerSpec));

module.exports = router;