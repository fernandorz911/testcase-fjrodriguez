const express = require('express');
const cors = require('cors');
const app = express();
const routes = require('./routes/getFiles');

//rutas a implementar 
app.use(cors());
app.use(routes);

app.listen(3000, () => {
  console.log('Servidor escuchando en el puerto 3000');
});